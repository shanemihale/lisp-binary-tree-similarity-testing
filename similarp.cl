;;; similarp.cl
;;; Shane Hale (smh1931@cs.rit.edu)
;;;
;;; Description:
;;;  Determines if two tree binary trees and determines if they are similar

;;;
;;; similarp
;;; Description:
;;;  Returns whether the trees are equal or similar
;;; Arguments:
;;;  tree1 - the first binary tree
;;;  tree2 - the second binary tree
;;; Returns:
;;;  Boolean value, T or NIL if the binary trees are equal, or similar
;;;
(defun similarp (tree1 tree2)
    (cond
        ;check if the trees are equal, or similar
        ((equalTree tree1 tree2) T)
        ((similarTree tree1 tree2) T)
        (T NIL)
    )
)

;;;
;;; equalTree
;;; Description:
;;;  Determines if the binary trees are equal
;;; Arguments:
;;;  tree1 - the first binary tree
;;;  tree2 - the second binary tree
;;; Returns:
;;;  Whether the binary trees are equal
;;;
(defun equalTree(tree1 tree2)
    (cond
        ;if the parent node values are different, return NIL
        ((null (eql (car tree1) (car tree2))) NIL)

        ;if the children nodes are the same, return T
        ((and (eql (cadr tree1) (cadr tree2))
         (eql (caadr tree1) (caadr tree2))) T)

        ;check the child nodes of the child nodes
        ((and (equalTree (cadr tree1) (cadr tree2))
         (equalTree (caddr tree1) (caddr tree2))) T)

        ;return NIL as a last resort
        (T NIL)
    )
)

;;;
;;; similarTree
;;; Description:
;;;  Determines if the binary trees are similar
;;; Arguments:
;;;  tree1 - the first binary tree
;;;  tree2 - the second binary tree
;;; Returns:
;;;  Whether the binary trees are similar
;;;
(defun similarTree(tree1 tree2)
    (cond
        ;if the parent node values are different, return NIL
        ((null (eql (car tree1) (car tree2))) NIL)
        
        ;if the first tree's right child equals the second tree's left child
        ((and (equalTree (cadr tree1) (caddr tree2))
         (equalTree (caddr tree1) (cadr tree2))) T)
        
        ;check the child nodes of the child nodes
        ((and (similarTree (cadr tree1) (cadr tree2))
              (similarTree (caddr tree1) (caddr tree2))
         ) T)
        
        ;return NIL as a last resort
        (T NIL)
    )
)
